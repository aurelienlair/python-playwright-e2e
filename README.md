# Python Playwright E2E tests

The objective of this project is to offer a practical learning opportunity for conducting automated browser tests using [Playwright](https://playwright.dev/python/) in [Python](https://www.python.org) integrated with [Allure](https://allurereport.org/) reports. Within this repository, you'll encounter comprehensive guidelines, code excerpts, and setups for constructing and executing automated browser tests.

## Table of Contents

1. [Pre-requirements](#pre-requirements)
2. [Installation](#installation)
3. [Set Up a virtual environment](#set-up-a-virtual-environment)
4. [Running TUI Experiences E2E tests with Chromium](#running-tui-experiences-e2e-tests-with-chromium)
   1. [Executing Tests in a Headed Browser](#executing-tests-in-a-headed-browser)
   2. [Executing Tests in a Headless Browser](#executing-tests-in-a-headless-browser)
   3. [Generate and Open Allure report](#generate-and-open-allure-report)
   4. [Executing Tests in the CI/CD pipeline](#executing-tests-in-the-cicd-pipeline)
5. [Other useful commands](#other-useful-commands)
   1. [Run test with debugger](#run-test-with-debugger)
   2. [Run a specific test with grep](#run-a-specific-test-with-grep)
   3. [Run tests on a specific browser](#run-tests-on-a-specific-browser)
   4. [Playwright tracing](#playwright-tracing)
   5. [Codegen](#codegen)
6. [Code Quality and Formatting Tools](#code-quality-and-formatting-tools)
   1. [Flake8](#flake8)
   2. [Black](#black)
   3. [Pre-commit](#pre-commit)

## Pre-requirements

To get started with this project, you'll need to install the following tools:

- [Python](https://www.python.org/): Python is a programming language used for writing test scripts and managing dependencies.
- [Pipenv](https://pipenv.pypa.io/en/latest/): Pipenv is a package management tool for Python that provides a convenient and reliable way to manage project dependencies and create virtual environments.
- [Visual Studio Code](https://code.visualstudio.com/): Visual Studio Code is a lightweight and powerful code editor with great support for Python development and a wide range of extensions.
- [Git](https://git-scm.com/): Git is a version control system used for tracking changes in your code and collaborating with others.
- [Allure](https://allurereport.org/docs/gettingstarted-installation/): Allure is a reporting tool that provides visually appealing and interactive test execution reports, enhancing visibility and analysis of test results for various test frameworks.

Please follow the links provided for each tool to access the respective installation instructions for your specific operating system. Once you have these tools installed, you'll be ready to proceed with the experimentation project.

## Installation

This chapter guides you through the installation process, ensuring that you have all the necessary tools and dependencies to run the project.

### Set Up a virtual environment

Establish the virtual environment and install Python requirements. The virtual environment will be automatically installed within a directory named `.venv` by default.

```bash
make install-virtual-env
```

And for activating the virtual environment

```bash
make activate-virtual-env
```

If necessary, you can still uninstall it by executing

```bash
make remove-virtual-env
```

Subsequently, you can [install](https://playwright.dev/python/docs/browsers) the default browsers supported by Playwright (only during the initial launch) using the following command:

```bash
playwright install
```

## Running TUI Experiences E2E tests with Chromium

### Executing Tests in a Headed Browser

To run the test suite from the command line in a headed browser (Chromium) for visualizing the execution of each test, utilize the following command.To execute the test suite from the command line in a headed browser (Chromium), that is to visualise the execution of each test, use the following command:

```bash
make run-tests
```

ℹ️ This mode does not produce Allure reports.

### Executing Tests in a Headless Browser

To run the test suite from the command line in a [headless browser](https://en.wikipedia.org/wiki/Headless_browser) (Chromium) and generate a comprehensive Allure report for each test, use the following command:

```bash
make run-tests
```

ℹ️ This mode precisely replicates the identical steps of the [CI/CD](https://en.wikipedia.org/wiki/CI/CD) pipeline. It will save necessary data into `allure-results`. If the directory already exists, the new files will be added to the existing ones, so that a future report will be based on them all.

### Generate and Open Allure report

Following the test execution, you can view the outcome details within the Allure report. To achieve this, generate a new Allure report promptly:

```bash
make allure-generate-report
```

ℹ️ If you have generated other Allure reports previously, the provided command will clear its report directory before creating a new one.

Now, just run the following command to open the generated report:

```bash
make allure-open-report

```

<img src="./docs/images/allure_report.png" alt="allure_report" style="width:80%;"/>

### Executing Tests in the CI/CD pipeline

Whenever a commit is pushed, a pipeline is initiated executing the entire test suite in [headless mode](https://en.wikipedia.org/wiki/Headless_browser) and generating an immediate Allure report. If a test fails, the report will include a Playwright trace as an attachment, which can be downloaded and utilized with the Playwright [Trace Viewer](https://playwright.dev/docs/trace-viewer). This proves to be highly beneficial for debugging purposes.

<img src="./docs/images/pipeline.png" alt="pipeline" style="width:80%;"/>

You can view the reports including their history, generated in the GitLab pipeline by visiting the [following](https://aurelienlair.gitlab.io/python-playwright-e2e/) URL.

##  Other useful commands

### Run test with debugger

It is possible to run the test suite with a [debugger](https://playwright.dev/python/docs/debug) so that every single step can be debugged. Below an example of how to run the test suite on Chromium with the debugger.

```bash
PWDEBUG=1 pytest
```

### Run a specific test with grep

In command line it is possible to specify which test(s) to run by adding Pytest [-k](https://docs.pytest.org/en/latest/how-to/usage.html#specifying-tests-selecting-tests) option.

```bash
pytest -k 'city page'
```

### Run tests on a specific browser

```bash
pytest --browser 'webkit'
```

### Playwright tracing

```bash
playwright show-trace  test-results/tests-test-search-experiences-suggestions-py-test-search-suggested-experiences-chromium/trace.zip
```

### Codegen

It is possible to record manual actions within a pages by using the [codegen](https://playwright.dev/python/docs/codegen-intro#running-codegen) command.

```bash
playwright codegen 'https://www.tuiexperiences.com/us/'
```

Every single action will be tracked as code, once finished just click on the "Record" button to save it.

<img src="./docs/images/codegen.png" alt="codegen" style="width:80%;"/>

## Code Quality and Formatting Tools

### Flake8

[Flake8](https://flake8.pycqa.org/en/latest/) serves as a Python linting tool designed to assess Python code for compliance with coding standards and to pinpoint potential errors and issues related to code quality. When applied, Flake8 evaluates Python code against multiple coding standards and guidelines, encompassing [PEP 8](https://peps.python.org/pep-0008/). It detects various concerns such as syntax errors, undefined variables, unused imports, code complexity, and more. Detailed explanations of [error codes](https://pycodestyle.pycqa.org/en/latest/intro.html#error-codes) can be referenced for further understanding.

Execute it in the local environment by running:

```bash
make lint
```

ℹ️ It is automatically executed in the [CI/CD](https://en.wikipedia.org/wiki/CI/CD) pipeline.

### Black

[Black](https://black.readthedocs.io/en/stable/index.html) is a Python code formatter designed to automatically adjust Python code, ensuring adherence to a consistent and opinionated coding style.

Unlike linting tools, Black is solely focused on code formatting. It imposes a specific set of rules and conventions related to code layout, indentation, line length, and other formatting aspects.

For additional information, refer to the Black command line [options](https://black.readthedocs.io/en/stable/usage_and_configuration/the_basics.html#command-line-options).

Black is compatible with configurations in various formats for common tools. In this project, I have integrated it with Flake8 and Pre-commit. For more details, please refer to the following pages:

- [Black with Flake8](https://black.readthedocs.io/en/stable/guides/using_black_with_other_tools.html#flake8)
- [Black with Pre-commit](https://black.readthedocs.io/en/stable/integrations/source_version_control.html)

To verify if there are some files to be formatted (dry-run) execute:

```bash
make format-check
```

To preview the formatting changes that will be applied to the code, execute the following:

```bash
make format-diff
```

Execute to apply the code formatting changes:

```bash
make format
```

ℹ️ Black is automatically executed in the [CI/CD](https://en.wikipedia.org/wiki/CI/CD) pipeline.

### Pre-commit

[Pre-commit](https://pre-commit.com/#intro) proves valuable in identifying straightforward issues before code undergoes review. By performing checks and actions prior to committing code, it aids in early issue detection during development, diminishing the chance of bugs and ensuring adherence to project-specific coding standards. If you wish to learn more about the installation process, refer to the [following](https://pre-commit.com/#3-install-the-git-hook-scripts) page.

To install pre-commit during the initial setup, execute:

```bash
make install-pre-commit-hook
```

Henceforth, if you attempt to commit a file with formatting errors or non-compliance with Python [PEP standards](https://peps.python.org/pep-0008/), the commit will be unsuccessful.

### Mypy

In this project, I am using [Mypy](https://mypy-lang.org/) an optional [static type checker](https://en.wikipedia.org/wiki/Type_system#Type_checking) for Python. It seeks to integrate the advantages of dynamic (or "duck") typing with static typing.

Run the following command to perform static code analysis with Mypy on the codebase:

```bash
make typecheck
```

ℹ️ Unlike Flake8 or Black, Mypy intentionally hasn't been included as a pre-commit hook. This decision stems from the fact that when Mypy attempts to type-check code before committing, in a nutshell it uses a virtual environment. I observed that running Mypy on a virtual environment not strictly identical to the one used in the current project resulted in numerous errors or false positives, prompting me to deliberately discard the idea. Nonetheless, Mypy is automatically run in the [CI/CD](https://en.wikipedia.org/wiki/CI/CD) pipeline.

## Git commit message convention

This projects follows the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/).

```shell
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

Example:

```shell
docs: add description of TypeScript run command
```

| Type         | Description                                                                                            |
| ------------ | ------------------------------------------------------------------------------------------------------ |
| `style`    | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc) |
| `build`    | Changes to the build process                                                                           |
| `chore`    | Changes to the build process or auxiliary tools and libraries such as documentation generation         |
| `docs`     | Documentation updates                                                                                  |
| `feat`     | New features                                                                                           |
| `fix`      | Bug fixes                                                                                              |
| `refactor` | Code refactoring                                                                                       |
| `test`     | Adding missing tests                                                                                   |
| `perf`     | A code change that improves performance                                                                |

## Useful links

- [Playwright](https://playwright.dev)
- Playwright [CI/CD](https://playwright.dev/docs/ci#gitlab-ci)
- Pytest Allure [plugin](https://pypi.org/project/allure-pytest/)
- [Flake8](https://flake8.pycqa.org/en/latest/)
- [Black](https://black.readthedocs.io/en/stable/index.html)
- [Pre-commit](https://pre-commit.com/#intro)
- [Mypy](https://mypy-lang.org/)
- [Object repository](https://www.browserstack.com/guide/object-repository-in-selenium#:~:text=in%20Selenium%20Script-,What%20is%20an%20Object%20Repository%20in%20Selenium%3F,properties%20in%20Selenium.) pattern
- [Page Object Model with Playwright](https://andrewbayd.medium.com/page-object-model-with-playwright-f8fbd5e8fa0f)
