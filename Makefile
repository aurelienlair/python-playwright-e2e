.DEFAULT_GOAL:=help
CUR_DIR:=$$(pwd)
SHELL ?= /bin/zsh
ALLURE_DIR ?= allure-results
JUNIT_REPORT_DIR ?= junit-report
export PIPENV_VENV_IN_PROJECT=true

help:
	@echo "❓ helps section"
	@grep -E '^[a-zA-Z_-]+.*?## .*$$' Makefile | sed 's/:.*##/##/' | sort | awk 'BEGIN {FS = "## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

activate-virtual-env: ## 🔌   Activate Python virtual environment
	@echo "🔌 activating python virtual environment"
	@if [ -z "$$VIRTUAL_ENV" ]; then \
		pipenv shell; \
	else \
		echo "Virtual environment is already activated."; \
	fi

allure-generate-report: ## 🔌   TODO
	@echo "🔌 allure-generate-report"
	allure generate --clean  allure-results --output allure-report

allure-open-report: ## 🔌   TODO
	@echo "🔌 open allure-report"	
	allure open allure-report

echo-virtual-env: ## 🗨️    Echo Python virtual environment
	@echo "🐍 Python virtual environment present in $(VIRTUAL_ENV)"

format: ## 🧰   Apply code formatting changes
	@echo "🔧 Running Black to auto-format Python code..."
	pipenv run black .

format-check: ## 👁️‍🗨️    Check possible code formatting changes (dry-run)
	@echo "🔧 Running Black auto-format Python code check (dry-run)..."
	pipenv run black --check . || true

format-check-ci: ## 👁️‍🗨️    Check possible code formatting changes in CI/CD
	@echo "🔧 Running Black auto-format Python code check in CI/CD..."
	pipenv run black --check .

format-diff: ## 🔄   Preview code formatting changes (dry-run)
	@echo "🔧 Running Black auto-format Python code preview (diff)..."
	pipenv run black --diff .	|| true

install-pre-commit-hook: ## 🪝   Install Git pre-commit hook
	@echo "🔌 Installing Git pre-commit hook..."
	pipenv shell pre-commit install

install-virtual-env: ## 🛠️    Install Python virtual environment
	@echo "🌐 Installing python virtual environment"
	pipenv install --dev

install-virtual-env-ci: ## 🛠️    Install Python virtual environment in CI/CD
	@echo "🌐 Installing python virtual environment in CI/CD"
	pip install pipenv
	pipenv install --deploy --dev

lint: ## 🚀   Perform lint check with Flake8
	@echo "🔍 Running Flake8 to check code style and quality...."
	pipenv run flake8 . || true

lint-ci: ## 🚀   Perform lint check with Flake8 in CI/CD
	@echo "🔍 Running Flake8 to check code style and quality in CI/CD...."
	pipenv run flake8

pre-commit-all: ## 🚦   Run pre-commit hooks against the codebase
	@echo "🏁 Running pre-commit hooks on the whole codebase"
	pipenv run pre-commit run --all-files --verbose

remove-pre-commit-hook: ## 🚫   Remove Git pre-commit hook
	@echo "🧹 Removing Git pre-commit hook..."
	rm -rvf .git/hooks/pre-commit

remove-virtual-env: ## 🗑️🛠   Remove Python virtual environment
	@echo "🌐 removing python virtual environment"
	-pipenv --rm 2>/dev/null || true
	(unset PIPENV_VENV_IN_PROJECT; \
    unset VIRTUAL_ENV; \
    unset VIRTUAL_ENV_PROMPT; \
    echo "Environment variables unset in this shell")

run-tests: ## ✅🧪 Running tests
	@echo "✅🧪 Running tests"
	pipenv run pytest --headed --verbose

run-tests-ci: ## ✅🧪 Running tests in CI/CD
	@echo "✅🧪 Running tests in CI/CD"
	pipenv run pytest --numprocesses 2 --alluredir=./${ALLURE_DIR} --junitxml=${JUNIT_REPORT_DIR}/report.xml

typecheck: ## 🧐 Run type checking
	@echo "🔍 Running mypy to analyze code for potential type issues..."
	pipenv run mypy --pretty . || true

typecheck-ci: ## 🧐 Run type checking in CI/CD
	@echo "🔍 Running mypy to analyze code for potential type issues in CI/CD...."
	pipenv run mypy --pretty .

show-packages-graph: ## 📊   Show Python packages graph
	@echo "📈 showing Python packages graph"
	pipenv graph
