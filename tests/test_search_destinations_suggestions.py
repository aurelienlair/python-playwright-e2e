from playwright.sync_api import Route
from pages.home_page import HomePage
from object_repository.urls.home_page_urls import autocomplete_url
from object_repository.input_data.home_page_input_data import HomePageInputData
from object_repository.expected_data.home_page_expected_data import (
    HomePageExpectedData,
)
import json


def handler(route: Route) -> None:
    with open("payloads/suggestions_payload.json") as json_file:
        data = json.load(json_file)
        route.fulfill(json=data)


def test_when_i_search_for_a_destination_with_experiences_it_displays_it_as_suggestion(
    home_page: HomePage,
) -> None:
    home_page.page.route(
        autocomplete_url(HomePageInputData.destination), handler
    )
    home_page.open()
    home_page.fill_search_input(HomePageInputData.destination)

    home_page.suggestions_contains_destination(
        HomePageExpectedData.suggested_destination
    )


def test_when_i_search_for_a_destination_without_experiences_it_does_not_displays_it_as_suggestion(
    home_page: HomePage,
) -> None:
    home_page.page.route(
        autocomplete_url(HomePageInputData.destination), handler
    )
    home_page.open()
    home_page.fill_search_input(HomePageInputData.destination)

    home_page.suggestions_does_not_contain_destination(
        HomePageExpectedData.not_expected_suggested_destination
    )
