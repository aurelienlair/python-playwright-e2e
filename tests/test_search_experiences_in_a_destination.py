from playwright.sync_api import Route
from pages.home_page import HomePage
from pages.experiences_page import ExperiencesPage
from object_repository.expected_data.experiences_page_expected_data import (
    ExperiencesPageExpectedData,
)
from object_repository.input_data.experiences_page_input_data import (
    ExperiencesPageInputData,
)
from object_repository.urls.experiences_page_urls import search_experiences_url
import pytest
import json


def test_search_experiences_in_a_specific_destination_redirect_to_proper_url(
    home_page: HomePage,
) -> None:
    home_page.open()
    experiences_page = home_page.perform_search(
        ExperiencesPageInputData.destination
    )
    experiences_page.title_contains(ExperiencesPageExpectedData.page_title)


@pytest.mark.xfail(
    reason="This test is failing because the returned title does not contain any searched query"
)
def test_experiences_page_in_specific_destination_should_have_a_proper_title(
    home_page: HomePage,
) -> None:
    home_page.open()
    experiences_page = home_page.perform_search(
        ExperiencesPageInputData.destination
    )
    experiences_page.header_title_contains(
        ExperiencesPageExpectedData.header_title
    )


def test_search_experiences_in_a_specific_destination_shows_experiences_related_with_that_destination(
    home_page: HomePage,
) -> None:
    home_page.open()

    def handler(route: Route) -> None:
        with open("payloads/experiences_payload.json") as json_file:
            data = json.load(json_file)
            route.fulfill(json=data)

    home_page.page.route(
        search_experiences_url(ExperiencesPageInputData.destination), handler
    )
    experiences_page = home_page.perform_search(
        ExperiencesPageInputData.destination
    )
    experiences_page.first_experience_in_results_contains(
        ExperiencesPageExpectedData.first_experience_name
    )


def test_filtering_experiences_by_language_in_specific_destination(
    experiences_page: ExperiencesPage,
) -> None:
    experiences_page.navigate(value=ExperiencesPageInputData.destination)
    experiences_page.decline_cookies()
    experiences_page.filter_by_language(ExperiencesPageInputData.language)
    experiences_page.expect_experiences_to_have_language_available(
        ExperiencesPageExpectedData.language_code
    )
