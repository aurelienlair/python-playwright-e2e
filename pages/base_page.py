from object_repository.locators.base_page_locators import BasePageLocator
from playwright.sync_api import Page


class BasePage:
    def __init__(self, page: Page):
        self.page = page

    def open(self, url: str = "/") -> None:
        self.page.goto(url)
        self.decline_cookies()

    def decline_cookies(self) -> None:
        self.page.locator(BasePageLocator.cookie_banner).click()
