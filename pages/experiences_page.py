from pages.base_page import BasePage
from playwright.sync_api import Page, expect
from object_repository.locators.experiences_page_locators import (
    ExperiencesPageLocator,
)
import re


class ExperiencesPage(BasePage):
    def __init__(self, page: Page):
        super().__init__(page)
        self.page_header_title = page.locator(
            ExperiencesPageLocator.page_header_title
        )
        self.first_experience_in_results = page.locator(
            ExperiencesPageLocator.first_experience_in_experiences_results
        )
        self.languages_filters = page.locator(
            ExperiencesPageLocator.languages_filter
        )
        self.available_languages_in_experiences_results = page.locator(
            ExperiencesPageLocator.available_languages_in_experiences_results
        )

    def navigate(self, value: str) -> None:
        self.page.goto(f"/search/?text={value}")

    def header_title_contains(self, value: str) -> None:
        expect(self.page).to_have_title(
            re.compile(rf"Results for your search '{value}'")
        )

    def title_contains(self, value: str) -> None:
        expect(self.page_header_title).to_contain_text(
            re.compile(rf"searched.*{value}")
        )

    def first_experience_in_results_contains(self, value: str) -> None:
        expect(self.first_experience_in_results).to_contain_text(value)

    def filter_by_language(self, language: str) -> None:
        self.page.get_by_role("heading", name="Activity languages").click()
        self.languages_filters.get_by_text(language).click()

    def expect_experiences_to_have_language_available(
        self, language_code: str
    ) -> None:
        for (
            available_languages_in_experience
        ) in self.available_languages_in_experiences_results.all():
            expect(available_languages_in_experience).to_contain_text(
                language_code
            )
