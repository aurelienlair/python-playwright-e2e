from pages.base_page import BasePage
from pages.experiences_page import ExperiencesPage
from playwright.sync_api import Page, expect
from object_repository.locators.home_page_locators import HomePageLocator
from object_repository.urls.experiences_page_urls import search_url


class HomePage(BasePage):
    def __init__(self, page: Page):
        super().__init__(page)
        self.search_component_input = self.page.get_by_test_id(
            HomePageLocator.search_component_input
        )
        self.search_component_button = self.page.get_by_test_id(
            HomePageLocator.search_component_button
        )

    def fill_search_input(self, input: str) -> None:
        self.search_component_input.click()
        self.search_component_input.fill(input)

    def perform_search(self, input: str) -> ExperiencesPage:
        self.fill_search_input(input)
        self.search_component_button.click()
        self.page.wait_for_url(search_url(input))

        return ExperiencesPage(self.page)

    def suggestions_contains_destination(self, destination: str) -> None:
        expect(
            self.page.get_by_test_id(HomePageLocator.search_bar)
            .filter(has=self.page.get_by_text("Destinations", exact=True))
            .filter(has=self.page.get_by_text(destination, exact=True))
        ).to_be_visible()

    def suggestions_does_not_contain_destination(
        self, destination: str
    ) -> None:
        expect(
            self.page.get_by_test_id(HomePageLocator.search_bar).get_by_text(
                destination
            )
        ).not_to_be_visible()
