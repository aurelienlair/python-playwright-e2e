from enum import StrEnum


class HomePageExpectedData(StrEnum):
    suggested_destination = "Paris"
    not_expected_suggested_destination = "London"
