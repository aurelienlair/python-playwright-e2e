from enum import StrEnum


class ExperiencesPageExpectedData(StrEnum):
    header_title = "Paris"
    page_title = "paris"
    first_experience_name = "FAKE INJECTED EXPERIENCE"
    language_code = "en"
