from enum import StrEnum


class ExperiencesPageInputData(StrEnum):
    destination = "paris"
    language = "English"
