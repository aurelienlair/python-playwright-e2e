def autocomplete_url(input: str) -> str:
    return f"*/**/autocomplete*text={input}*"
