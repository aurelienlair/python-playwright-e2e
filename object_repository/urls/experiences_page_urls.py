def search_url(input: str) -> str:
    return f"**/search/?text={input}*"


def search_experiences_url(input: str) -> str:
    return f"*/**/searchcomponent/activities*text={input}*"
