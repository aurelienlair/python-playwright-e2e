from enum import StrEnum


class HomePageLocator(StrEnum):
    search_component_input = "search-component-central-search-body"
    search_component_button = "search-component-search-button"
    search_bar = "search-component-search-bar"
