from enum import StrEnum


class BasePageLocator(StrEnum):
    cookie_banner = "[data-test='cookie-banner__accept-cookies']"
