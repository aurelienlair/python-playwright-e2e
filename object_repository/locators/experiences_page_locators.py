from enum import StrEnum


class ExperiencesPageLocator(StrEnum):
    page_header_title = '[data-test-id="page-header-title"]'
    first_experience_in_experiences_results = '[data-test-id="ResultsList__activity-card-0"] [data-test="ActivityCard__title-link"]'
    languages_filter = '[data-test="searchComponent_filters"]'
    available_languages_in_experiences_results = (
        '[data-test="ActivityCard"] [data-test="spanLangFather"]'
    )
