stages:
  - run-test-suite
  - download-and-copy-history
  - generate-report
  - publish-report

variables:
  ALLURE_DIR: "allure-results"
  PYTHON_VERSION: "3.11"

tests:
  stage: run-test-suite
  image: python:${PYTHON_VERSION}-slim
  variables:
    ALLURE_RESULTS_DIR: $ALLURE_DIR
  before_script:
    - apt-get update && apt-get -qqy --no-install-recommends install make
  script:
    - make install-virtual-env-ci
    - make format-check-ci
    - make lint-ci
    - make typecheck-ci
    - pipenv run playwright install-deps
    - pipenv run playwright install
    - make run-tests-ci
  allow_failure: true
  artifacts:
    when: always
    paths:
      - ./${ALLURE_DIR}
      - ./test-results
    expire_in: 1 day
    reports:
      junit: reports/report.xml

download-and-copy-history:
  stage: download-and-copy-history
  tags:
    - docker
  image: storytel/alpine-bash-curl
  script:
    - |
      # For private projects `--header "PRIVATE-TOKEN: ${CI_JOB_TOKEN}"` is only available with premium subscriptions
      # see https://eokulik.com/gitlab-ci-download-job-artifacts-from-a-private-repository/
      # for free private versions use `--header "PRIVATE-TOKEN: <your_access_token>"`
      curl -s --location \
        --output artifacts.zip \
        "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/jobs/artifacts/${CI_COMMIT_BRANCH}/download?job=pages"
      apk add unzip
      unzip artifacts.zip -d ./allure-report-previous
      cp -r ./allure-report-previous/public/history ./${ALLURE_DIR}
  allow_failure: true
  artifacts:
    when: always
    paths:
      - ./${ALLURE_DIR}
    expire_in: 1 day

generate-report:
  stage: generate-report
  tags:
    - docker
  image: frankescobar/allure-docker-service
  script:
    - allure generate -c ./${ALLURE_DIR} -o ./${ALLURE_REPORT_DIR}
  artifacts:
    when: always
    paths:
      - ./${ALLURE_REPORT_DIR}
    expire_in: 1 day

pages:
  stage: publish-report
  needs:
    - job: generate-report
      artifacts: true
  script:
    - mv ${ALLURE_REPORT_DIR} public/
  artifacts:
    paths:
      - public
    expire_in: never
    when: always
