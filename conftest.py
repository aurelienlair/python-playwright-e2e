import pytest
from slugify import slugify
from playwright.sync_api import Page, BrowserContext
from pages.home_page import HomePage
from pages.experiences_page import ExperiencesPage
from typing import Dict, Any, Generator
import allure
import os
import hashlib


@pytest.fixture(scope="session")
def browser_context_args(
    browser_context_args: Dict[str, Any]
) -> Dict[str, Any]:
    return {
        **browser_context_args,
        "geolocation": {"longitude": 62.3839, "latitude": -59.9142},
        "permissions": ["geolocation"],
    }


@pytest.fixture(scope="function")
def home_page(page: Page) -> HomePage:
    return HomePage(page)


@pytest.fixture(scope="function")
def experiences_page(page: Page) -> ExperiencesPage:
    return ExperiencesPage(page)


def _build_artifact_test_folder(
    pytestconfig: Any, request: pytest.FixtureRequest, folder_or_file_name: str
) -> str:
    output_dir = pytestconfig.getoption("--output")
    return os.path.join(
        output_dir,
        truncate_file_name(slugify(request.node.nodeid)),
        truncate_file_name(folder_or_file_name),
    )


def truncate_file_name(file_name: str) -> str:
    if len(file_name) < 256:
        return file_name
    return f"{file_name[:100]}-{hashlib.sha256(file_name.encode()).hexdigest()[:7]}-{file_name[-100:]}"


@pytest.fixture(scope="function")
def context(
    context: BrowserContext,
    request: pytest.FixtureRequest,
    pytestconfig: Any,
) -> Generator[BrowserContext, None, None]:
    # start tracing
    context.tracing.start(
        title=slugify(request.node.nodeid),
        screenshots=True,
        snapshots=True,
        sources=True,
    )
    yield context
    # stop tracing and attach on failure
    retain_trace = request.node.rep_call.failed
    if retain_trace:
        trace_path = _build_artifact_test_folder(
            pytestconfig, request, "trace.zip"
        )
        context.tracing.stop(path=trace_path)
        allure.attach.file(
            trace_path,
            name="trace.zip",
            extension="zip",
        )
    else:
        context.tracing.stop()


@pytest.fixture(autouse=True)
def page(
    page: Page, request: pytest.FixtureRequest
) -> Generator[Page, None, None]:
    yield page
    if request.node.rep_call.failed:
        png_bytes = page.screenshot()
        allure.attach(
            png_bytes,
            name="full-page",
            attachment_type=allure.attachment_type.PNG,
        )
